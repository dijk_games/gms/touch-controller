///@function  global_dragging()
///@description  Drag any object dragging at any point of your screen
///@author  Jesus Alvarado Bastida <jalvarado@dijkstracompany.com>
///@license	GPL 3.0 <https://www.gnu.org/licenses/gpl-3.0.html>
///@copyright  Dijkstra Company S.A.S de C.V
function global_dragging() constructor{
	oX = 0;
	oY = 0;
	onsStartDragging = function(_x,_y){
		oY = _y - event_data[?"posY"];
		oX = _x - event_data[?"posX"];
	}
	
	onDragEvent_getX = function(){
			return event_data[?"posX"] + oX;
	}
	
	onDragEvent_getY = function(){
			return event_data[?"posY"] + oY;
	}
}